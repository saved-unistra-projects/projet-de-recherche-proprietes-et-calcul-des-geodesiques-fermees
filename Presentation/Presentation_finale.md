---
title: "Propriétés et Calcul des Géodésiques Fermées"
output: beamer
theme: "Berlin"
colortheme: beaver
author: "Audric Besnault--Clérice, Raphaël Colin, Morgane Schieber"
header-includes: |
    \beamerdefaultoverlayspecification{<+->}
date: "Vendredi 10 Mai 2019"
---

# Introduction

## Qu'est-ce qu'une géodésique fermée ?

## Qu'est-ce qu'une géodésique fermée ?

\begin{figure}
\includegraphics[height=150pt]{figures/pichetbsans.png}
\caption{Un pichet}
\end{figure}

## Qu'est-ce qu'une géodésique fermée ?

\begin{figure}
\includegraphics[height=150pt]{figures/pichetbavec.png}
\caption{Une constriction sur ce pichet}
\end{figure}

## Domaines d'application


\begin{itemize}
\item<2-> Médecine
\end{itemize}

## Médecine

\begin{figure}
\includegraphics[height=150pt]{figures/veine2.png}
\caption{Une constriction calculée sur une veine [S. Bischoff et al., 2005]}
\end{figure}

## Domaines d'application 

\begin{itemize}
\item<1-> Médecine

\item<2-> Industrie pétrolière

\item<3-> Mécanique

\item<4-> Animation 3D
\end{itemize}

## Animation 3D

\begin{figure}
\includegraphics[height=150pt]{figures/articulation1.png}
\caption{Un squelette pour l'animation [J. Tierny et al., 2006]}
\end{figure}


## Animation 3D

\begin{figure}
\includegraphics[height=150pt]{figures/articulation2.png}
\caption{Des constrictions calculées sur le modèle [J. Tierny et al., 2006]}
\end{figure}

## Animation 3D

\begin{figure}
\includegraphics[height=150pt]{figures/articulation3.png}
\caption{Le squelette amélioré à l'aide des constrictions [J. Tierny et al., 2006]}
\end{figure}


# Définitions et enjeux informatiques

## Un espace mathématique continu

\begin{itemize}

\item<2-> Surface de Riemann

\item<3-> Beaucoup de propriétés
\end{itemize}

## Du continu au discret...


\begin{itemize}
\item<2-> Travail sur des maillages

\item<3-> Les propriétés ne sont plus les mêmes

\item<4-> Problème du \bf{bruit}
\end{itemize}

## Le bruit ?
\begin{figure}
\includegraphics[height=150pt]{figures/vase6b.png}
\caption{Illustration du problème du bruit}
\end{figure}

# Algorithmes

## Stratégie générale de calcul 

\begin{itemize}
\item<2-> Plusieurs stratégies de calcul

\item<3-> Une approche similaire

\item<4-> Pourtant les algorithmes restent différents
\end{itemize}

## Simplification de la surface

\begin{itemize}
\item<2-> Simplification de l'objet
\end{itemize}

## Simplification de la surface

\begin{figure}
\includegraphics[width=290pt]{figures/maillage_simplifie.png}
\caption{\footnotesize{Un triangle qui ne fait pas partie du maillage [F. Hetroy, D. Attali, 2003]}}
\end{figure}

## Simplification de la surface

\begin{itemize}
\item<1-> Simplification de l'objet

\item<2-> Reconstitution de l'objet
\end{itemize}

## Recherche à l'aide de la courbure

\begin{itemize}
\item<2-> Recherche de sommets candidats

\item<3-> Création de la courbe à l'aide d'un plan
\end{itemize}

## Tédraédrisation

\begin{itemize}
\item<2-> Tétraédrisation et transformation en membrane
\end{itemize}

## Tétraédrisation

\begin{figure}
\includegraphics[width=290pt]{figures/sortie_algo_fengtong.png}
\caption{Le procédé de tétraédrisation [X. Feng, Y. Tong, 2013]}
\end{figure}

## Tétraédrisation

\begin{itemize}
\item<1-> Tétraédrisation et transformation en membrane
\item<2-> Recherche de courbe initiale
\end{itemize}

## Algorithme de glissement

\begin{figure}
\includegraphics[height=150pt]{figures/glissement_xin_he_fu.png}
\caption{Le procédé de glissement [S.Q. Xin, Y. He, 2012]}
\end{figure}

## Algorithme de glissement

\begin{figure}
\includegraphics[height=150pt]{figures/glissement_hetroy_2003.png}
\caption{Les calculs effectués [F. Hetroy, D. Attali, 2003]}
\end{figure}

# Réflexions

## Réflexions

\begin{itemize}
\item<2-> Pourquoi rechercher une courbe initiale ?
\item<3-> Dépendant du domaine d'application ?
\item<4-> Comment définir ce qu'est le bruit ?
\end{itemize}

# Conclusion

## Conclusion

**Les géodésiques fermées :**

\begin{itemize}
\item<2-> Ont de nombreuses applications
\item<3-> Sont vouées à évoluer
\item<4-> Illustrent la traduction des mathématiques vers l'informatique
\end{itemize}

## Bibliographie

\tiny{
\begin{itemize}
\item<1-> CD.V. Anosov. Closed geodesic, Encyclopedia of Mathematics. Last modified 2011.

\item<1-> A. Blumentals. Rapport d'IRL: Calcul de Courbes Géodésiques Stables sur des Surfaces Maillées. 2012.

\item<1-> X. Feng and Y. Tong. Choking Loops on Surfaces. IEEE TRANSACTIONS ON VISUALIZATION AND COMPUTER GRAPHICS, VOL. 19, NO. 8, AUGUST 2013

\item<1-> F. Hétroy and D. Attali. Detection of constrictions on closed polyhedral surfaces. In Data Visualization 2003, Visualization Symposium Proc., pages 67–74, May 2003.

\item<1-> F. Hétroy and D. Attali. From a Closed Piecewise Geodesic to a Constriction on a Closed Triangulated Surface. In Paciﬁc Graphics 2003, pp. 394–398.

\item<1-> F. Hétroy. Constriction Computation using Surface Curvature. In Eurographics (short paper), Aug 2005, pp.1-4.

\item<1-> S. Xin and Y. He. Efficiently Computing Exact GeodesicLoops within Finite Steps. IEEE TRANSACTIONS ON VISUALIZATION AND COMPUTER GRAPHICS, VOL. 18, NO. 6, JUNE 2012 
\end{itemize}
}

