
---
title: "Propriétés et Calcul des Géodésiques Fermées"
output: beamer
theme: "Berlin"
colortheme: beaver
author: "Audric Besnault--Clérice, Raphaël Colin, Morgane Schieber"
date: "Vendredi 03 Mai 2019"
---

# Qu'est-ce qu'une géodésique fermée ?

Expliquer ce que c'est, passer aux constrictions, illustrer avec le pichet

max 1 min A

# Domaines d'application

Images si possible, parler assez vite des domaines d'application possible, 
dire qu'on y reviendra dessus quand on en saura plus sur les géodésiques.

max 1 min M

# (Problématique Continu/Discret => bruit)

Vite fait parler de maths 
max 30sec A

Puis parler de maillages et de bruit
max 1min30 R

# Stratégie générale de calcul
Dire qu'on a lu plein d'articles sur des méthodes de calcul, on a remarqué que 
ces méthodes utilisent un approche similaire, l'expliquer vite fait et dire 
qu'on va aborder les deux parties séparément
1 min M

# Courbe initiale
Faire des sous titres pour chaque algo

max 5min A R M:3

# Glissement

en citant l'article des chinois (au coeur de cette partie, pas juste à la fin) puis transition
max 2min30 A

# Pertinence de la recherche de courbe

On sait quoi dire on l'a déjà assez dit, reparler des domaines un peu

max 1min30 R

# Conclusion
Conclure en mode rapport stylé de ouf
max 1min R
