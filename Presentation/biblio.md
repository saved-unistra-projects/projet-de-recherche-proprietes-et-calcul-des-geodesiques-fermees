## Bibliographie

\begin{itemize}
\item<1-> https://www.encyclopediaofmath.org/index.php/Closed_geodesic
\item<1-> A. Blumentals. Rapport d'IRL: Calcul de Courbes Géodésiques Stables sur des Surfaces Maillées
\item<1-> X. Feng and Y. Tong. Choking Loops on Surfaces
\item<1-> F. Hétroy and D. Attali. Detection of constrictions on closed polyhedral surfaces
\item<1-> F. Hétroy and D. Attali. From a Closed Piecewise Geodesic to a Constriction ona Closed Triangulated Surface
\item<1-> F. Hétroy. Constriction Computation using Surface Curvature
\item<1-> S. Xin and Y. He. Efficiently Computing Exact GeodesicLoops within Finite Steps
\end{itemize}


