---
title: "Propriétés et Calcul des Géodésiques Fermées"
output: beamer
colortheme: beaver
author: "Audric Besnault--Clérice, Raphaël Colin, Morgane Schieber"
date: "Vendredi 15 Mars 2019"
---

## Qu'est-ce que c'est ?


### Les géodésiques

Expliquer concrètement ce que ça veut dire, prendre l'exemple des avions

R

### Les géodésiques fermées

![Géodésique fermée](Presentation/figures/geodesiqueresize.png)

Parler de minimum locaux

A

## Les enjeux

### L'intérêt de pouvoir les calculer

- Domaine pétrolier
Le truc des poches du prof

- Domaine médical
Détéction de rétrecissement de veines

- Animation
Points d'articulation

- ...
Faiblesse dans les systèmes mécaniques

**M**

### Difficulté des calculs

**Informatique $\neq$ Mathématiques :**

- Travail sur des maillages
Différence entre pratique et théorie, car on a pas de surface
parfaitement lisse en informatique

- Equilibre entre efficacité et temps de calcul
Self explanatory, faut trouver un juste milieu sachant que les
calculs sur les maillages sont coûteux en temps et mémoire

**R**

**Peu de chercheurs:**

- Manque de propriétés utiles pour les informaticiens
Les informaticiens on besoin de propriétés mathématiques pour faire des algos

**M**

## Recherches

### Point de départ


Rapport d'IRL: "Calcul de Courbes Géodésiques Stables sur des Surfaces Maillées"

*(A. Blumentals)*

Rapport de master, moins pointu que les autres articles, état de l'art. Ne parle pas d'un algo, présente ce qui se fait.

**A**

### Bases Mathématiques

"Closed geodesic"

*(D.V. Anosov Encyclopedia of Mathematics)*

**A**

### Bases Mathématiques

- Facilite la compréhension du reste des recherches

- Beaucoup de recherches annexes

**A**

### Articles scientifiques

- "From a Closed Piecewise Geodesic to a Constriction on a Closed Triangulated Surface"

Article de description d'algo, début des recherches en informatique sur le sujet, on en a fait une synthèse, le prof nous a aidé à le comprendre

- "Detection of Constrictions on Closed Polyhedral Surfaces"

**R** *A*

&nbsp;

*(Franck Hétroy, Dominique Attali)*


### Articles scientifiques

- "Choking Loops on Surfaces"

Article de description d'algo, base mathématique compliquée, algo un peu différent des autres, le prof a aussi aidé à comprendre pour celui-là, aussi synthétisé

*(Xin Feng et Yiying Tong (IEEE))*

**M**

### Méthodologie de travail

Lecture des articles, concertés pour les comprendre, rédaction de résumé d'algorithmes, recherche régulière de sources mathématiques pour comprendre (et le prof), article qui font référence à d'autres qui permettent d'apporter ou compléter des trucs

**M**

## Critique

### Profondeur mathématique

Nous avons approfondi les recherches et la compréhension sur les algorithmes même si certaines notions mathématiques ne sont pas parfaitement comprises car assez pointues

**M**

### Etendue des recherches

Le sujet est très ciblé, peu de chercheur, nous avons une idée assez large du sujet même si nous n'avons pas encore exploré certain détails

**R**

### Finalisation de l'étude

- Plus de recherches sur les mathématiques

- Synthèse d'algorithmes pour la présentation

*A* *M* *R*
