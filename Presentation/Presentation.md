---
title: "Propriétés et Calcul des Géodésiques Fermées"
output: beamer
theme: "Berlin"
colortheme: beaver
author: "Audric Besnault--Clérice, Raphaël Colin, Morgane Schieber"
date: "Vendredi 15 Mars 2019"
---

## Qu'est-ce que c'est ?


### Les géodésiques

Équivalent d'une ligne droite sur un espace courbe 

(plus court chemin)

![Géodésique](Presentation/figures/avion2.png)

### Les géodésiques fermées


C'est une géodésique qui décrit une boucle

&nbsp;

![Géodésique fermée](Presentation/figures/donut2.png)


### Les géodésiques fermées

![Constriction](Presentation/figures/image2.png)

## Les enjeux 

### L'intérêt de pouvoir les calculer

- Domaine pétrolier

- Domaine médical

- Animation

- ...

### Difficulté des calculs

**Informatique $\neq$ Mathématiques :**

- Travail sur des maillages

- Equilibre entre efficacité et temps de calcul

**Peu de chercheurs:**

- Manque de propriétés utiles pour les informaticiens

## Recherches

### Point de départ


Rapport d'IRL: "Calcul de Courbes Géodésiques Stables sur des Surfaces Maillées"

*(A. Blumentals)*

- Rapport de master

- État de l'art

### Bases Mathématiques

"Closed geodesic"

*(D.V. Anosov Encyclopedia of Mathematics)*

- Facilite la compréhension du reste des recherches

- Beaucoup de recherches annexes

### Articles scientifiques

- "From a Closed Piecewise Geodesic to a Constriction on a Closed Triangulated Surface"

- "Detection of Constrictions on Closed Polyhedral Surfaces"

*(Franck Hétroy, Dominique Attali)*

- Description/Présentation d'algorithme

- Début des recherches en informatique sur le sujet

- Beaucoup d'autres articles les citent

### Articles scientifiques

"Choking Loops on Surfaces"

*(Xin Feng et Yiying Tong (IEEE))*

- Bases mathématiques compliquées

- Se base sur les articles plus anciens

### Méthodologie de travail

- Articles souvent compris en groupe

- Synthèse d'algorithmes

- Recherches de sources sur les mathématiques


## Critique

### Profondeur et étendu des recherches

- Profondeur mathématique


- Etendue des recherches


### Finalisation de l'étude

- Plus de recherches sur les mathématiques

- Synthèse d'algorithmes pour la présentation
