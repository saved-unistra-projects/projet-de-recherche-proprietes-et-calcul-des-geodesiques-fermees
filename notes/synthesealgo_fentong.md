## Sythèse algorithme

### Choking Loops on Surfaces (Xi Feng and Yiying Tong, iEEE)

#### Détection de topologies non-triviales
**Préprocess:** Tétrahédralisation du maillage, détermination de la carte des distances (trois façons: *Closest point transform*, l'*algorithme de Dijkstra* ou le *fast matching*)
**Entrée:** Un maillage d'objet
**Résumé:** On filtre le maillage, une fois que tout est passé dans la filtration on obtient des homologies. On ajoute les sommets de l'homologie à la filtration, le sommet avec le plus petit $d$ (voir ci-dessous) et ses faces connectées, les simplices du maillage. On répète tout ça jusqu'à ce qu'on obtienne un $d_{max}$. Toutes les faces qui on une surface supérieure à $\delta_{max}$ et leur paire négative seront des *choke face*.
**Sortie:** Une membrane

**Valeurs notables & définitions:**
* $d$: distance de chaque sommet à la surface (enregistré à chaque sommet intérieur)
* $\delta$: persistance de l'objet/face/... qui ignore le bruit (car les maillages sont imparfaits)
* $p-simplices$: regroupement d'objet géométrique p+1, 0 pour les sommets, 1 pour les arrêtes, 2 pour les faces et 3 pour les tétrahèdres

#### Calcul des loops associées
**Entrée:** La membrane obtenue précédemment
**Résumé:** On transforme la membrane jusqu'à avoir une membrane minimale. On ajoute une *choke face* avec son groupe à la membrane.
* On prend le sommet avec le plus grand $d$ de la $boundary$
* On trouve une membrane locale optimale
* On fusionne les membranes.
On répète ces étapes jusqu'à ce que les faces de la $boundary$ soient à la surface.
**Sortie:** Les géodésiques associées à la membrane

**Valeurs notables & définitions:**
* $d$: distance de chaque sommet à la surface (enregistré à chaque sommet intérieur)
* $\delta$: persistance de l'objet/face/... qui ignore le bruit (car les maillages sont imparfaits)
* $boundary$: en topologie c'est genre les points "au bord" de l'objet, qu'on peut "approcher" à la fois via l'intérieur et via l'extérieur)
