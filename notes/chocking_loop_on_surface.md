## Choking Loops on Surfaces (Xi Feng and Yiying Tong, iEEE)

### Bases

* nontrivial loop: courbe non-constante (??)
* tunnel loop: quand un objet est vide, les plus petites courbes de ses tunnels


*(g = genus = nombre de “trous”/tunnels dans l’objet) (genre comme dans les éponges) (je pense)*

**Important :** les courbes qui ne peuvent pas se serrer autour d’un objet sans les déformer = géodésique → ils ont des algorithmes qui permettent de les trouver et qui les répartissent en 2 catégories : celle sur les solides (g handles) et celles dans les parties creuses d’un objet (g tunnels).

2g c’est un groupe de courbes (beaucoup sont non-triviales) qui permet de créer une base importante dans la structure de l’objet (ça a à voir avec de l’homologie et de l’homotopie, genre déformer un objet qui restera le même) → ça signifie qu’en déformant l’objet la base de courbe peut changer donc on aura le même objet mais plus les mêmes courbes

Si j’ai bien compris, le truc de déformation c’est pour voir si dans le « voisinage » de l’objet y aura pas moyen de trouver des géodésiques parce que la surface de l’objet permet pas d’en trouver avec la méthode des courbes avec des contraintes ou quand sur une surface triviale on trouve des courbes non triviales.

C’est pratique de chercher les géodésiques dans les endroits creux des objets parce que ça permet de savoir si quelque chose pourrait être coincé dedans ou pas. Ou alors, ça permet de trouver les tunnels dans les objets et c’est utile en biologie (cf. ex de l’article les flux d’ions pour déterminer des fonctions biologiques en biomolécule, pour les systèmes nerveux)

### Mathématiques

* **Complexe simplificial** = concaténation de p-simplices = objet géométrique généralisant la notion de triangulation d'une surface -> ça se présente comme une graphe du coup

* **p-simplices** = regroupement d'objet géométrique p+1, 0 pour les sommets, 1 pour les arrêtes, 2 pour les faces et 3 pour les tétrahèdres

* **p-chains** = combinaison linéaire de p-simplices:
$\sum_{i}^{} a_iσ_i$
  * $a_i$ appartenant à $Z_{/2}$ un entier modulo
  * 2σ un p-simplexe

* **p-simplex** = généralisation du triangle à p-dimension (dans notre cas de 0 à 3).

On définit alors une frontière (*boundary* en topologie c'est genre les points "au bord" de l'objet, qu'on peut "approcher" à la fois via l'intérieur et via l'extérieur) pour chaque p-simplexe traversé par les sommets $v_0$ à $v_p$:
* $δ_p[v_0,....,v_p] = \sum_{i=0}^{p} [v_0, ...., \hat{v_i}, ....., v_p]$
$\hat{v_i}$ signifie que $v_i$ est omis.
* p-boundaries = $Im(δ_{p+1})$
* p-chains = $Ker(δ_{p})$ (le kernel (= noyau) de $δ_{p}$)
* La $p-ième$ homologie peut être définie par **le quotient** $H_p(K) = Ker(δ_p)/Im(δ_{p+1})$ est c'est certainement aussi un **groupe**. On utilise le quotient dès que les p-boundaries sont des p-chains.

$*$ 0-homology générateur → 1-cycles sont des loops
$*$ 1-homology générateur → set de non-trivial loops indépendantes

* **interpolation linéaire** = méthode (la plus simple) pour estimer la valeur prise par une une fonction continue entre deux points déterminés.

* **lignes de niveau** = go voir ici [wikipédia](https://fr.wikipedia.org/wiki/Ligne_de_niveau)

* **lower-star filtration** = on prend un complexe simplicial et une fonction (real-valued) définie sur les sommets. On suppose qu'on veut calculer les interpolations linéaires des homologies des lignes de niveaux de la fonction.
Ses diagrammes de persistance sont pareils que le résultat de la lower-star filtration

* **théorie de morse** = méthodes et techniques qui permettent d'étudier la topologie d'une variété différencielle avec les lignes de niveaux

Dans leur algorithme ils utilise la *lowerstar filtration*, défini sur les séquences imbriquées de complexe avec des simplices et une fonction de Morse (sans point critique - dégénératif? - (sans point où la dérivée vaut 0)).

*Important*, de paire avec l'algo de la ref 22, ils "tuent" toujours le cycle le plus jeune parmi tout ceux qui peuvent être tués par un simplex négatif (je sais pas comment on obtient un simplex négatif, peut être en tétréhédralisant et en simplifiant)
(J'ai pas tout compris comment ils "tuent" et quand c'est le moment le tuer)

Y a des simplices négatifs et positifs. Une face positive va se séparer en plusieurs constituants tandis qu'une face négative va devenir plus simple.

Y a une manip mathématique à faire (que j'essaye de comprendre mais elle est pas évidente) et qui ils obtiendront une membrane telle que, les endroits où elle est étouffée indiquent qu'il y a une géodésique dans la zone de l'étouffement (pas forcément dessus, mais en tout cas, proche).

##### Calcul des Choking Loops

Basé sur une homologie de persistance et une mesh avec suffisamment de sommets intérieurs pour pouvoir détecter les chokings loops.
Par rapport à l'algo de HanTun (basé aussi sur l'homologie de persistance) mais avec une mesh qui a seulement des sommets sur sa surface.

### Algorithmique

Sur le papier ils ont trouvé un algorithme qui permettrait de trouver toutes les géodésiques « utiles » d’un objet (donc pas celles du 3e type).
L’algorithme travaille sur la/les base/s homologique/s d’un objet pour y trouver des courbes non-triviales. Ils utilisent des méthodes pour y parvenir comme la tétraédrisation de l’intérieur/extérieur de l’objet, l’algorithme de HanTun est une des premières méthodes de ce genre.

En gros, ils tétrahèdralisent la surface avec leur algo et dès qu'un passage d'air se voit *étouffé* ils pensent qu'il y a une géodésique à cet endroit.

*(++ y a un algorithme du plus court chemin qui existe, c'est l'Algorithme de Dijkstra)*

**Detection de topologies non-triviales: Etapes**
*Avant:*
* Lancement de la filtration, preprocess de la représentation de la surface
* Si "mesh triangle" ils font la tétrahedralisation (avec TetGen)
* Ils déterminent [(la carte de distances)](https://en.wikipedia.org/wiki/Distance_transform) de différentes façons:
  * Closest Point Transform
  * Dijkstra's algorithm
  * Pour les surfaces "implicites": fast marching puis le marching cubes
* Tetrahédralisation + interpolation trilinéaire

*Du coup:*
* Faire la filtration (la lowerstar je pense) et dès que toute la surface est dedans -> obtention de 2g positive edges qui réprésentent les 2g homoly generetors
* Ajouter les sommets intérieures
* On ajoute les sommets qu'il y a sur les "boundary"
* On ajoute le sommet avec le plus petit $d$, suivi des simplices de la mesh tétrahédralisée, le nouveau sommet, les faces qui sont connectées au sommet du subcomplexe (simplificial) courant, les faces formées par les sommets, d'autres sommets (j'ai pas encore compris la différence entre ces sommets).
* On répète l'opération jusqu'à ce qu'on obtienne $d_{max}$
* Toutes les faces qui auront une surface supérieure à $\delta_{max}$ seront des *choke faces* et toutes les négatives pairées à une positive aussi
* **Contrairement à HanTun** ils ont besoin des paires entre les faces et les arrêtes.

$d$: distance de chaque sommet à la surface (enregistré à chaque sommet intérieur)
$\delta$: persistance de l'objet/face/... qui ignore le bruit (car les maillages sont imparfaits)

**Calcul des loops associées: Etapes**
Ils ont obtenu leur membrane avec leur choke face. Il la transforme jusqu'à au obtenir une membrane minimale qui touche les boundary.
1. Ajouter une choke face $f$ avec son groupe à la membrane
2. Prendre un sommet avec le plus grand $d$ de la boundary
3. Trouver une membrane locale optimale
4. Fusionner les membranes
5. Répeter les étapes 2 à 4 jusqu'à ce que les faces boundary de la membrane sont sur la surface.

#### Résultats
- L'offset $d_{max}$ montre combien ils vont à l'intérieur du volume (10%)
- $\delta_{min}$ pour la persistance minimum pour filtrer le bruit. (50%)
- Ce qui prend le plus de temps est la partie sur la paire d'homologie persistante, sinon les autres étapes prennent un peu moins d'une seconde
- Un des soucis de leur algo c'est que si y a deux géodésiques fermées trop proches l'une de l'autre, l'algo n'en donne qu'une

#### Notes à moi-même
- Pour les surfaces triviales où on trouve des courbes non-triviales: homotopie/homologie (on modifie un peu l’objet pour palier au problème)
- Tétraédrisation: On trétraèdrise l’objet
- L’algorithme de HanTun [(voir ce papier)](ftp://ftp-sop.inria.fr/prisme/dcohen/Papers/hantun-sig.pdf) qui permet de trouver des g-handle loops et des g-tunnel loops (chocking loop sur le solide et dans ses parties vides)


#### Liens annexes (qui peuvent être utiles)
* les travaux de Eric Colin de Verdière et Francis Lazarus
→ [Finding shortes non-trivial cycles in directed graphs on surfaces](http://monge.univ-mlv.fr/~colinde/pub/09directed.pdf)
→ [Tout leur travail](http://monge.univ-mlv.fr/~colinde/)
* [Greedy optimal homotopy and homology generetors](http://jeffe.cs.illinois.edu/pubs/pdf/gohog.pdf) par Jeff Rickson et Kim Whittlesey
