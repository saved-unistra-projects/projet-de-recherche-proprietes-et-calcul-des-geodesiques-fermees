# Projet de recherche

## Notes

### Closed geodesic (maths)[^1]

Une variété de Riemann est un espace courbe sur lequel on peux calculer des distances.
Ainsi pour effectuer des calcules de géodésiques, qui sont les plus courts chemins, les textes doivent spécifier qu'on se situedans une variété de Riemann.

Mathématiquement, on considère qu'on déplace la courbe en appliquant une force qui oblige la courbe à aller là où la surface est moins large.

Si la courbe est au même endroit aux temps $t=a$ et $t=b$, (avec $a < b$) et qu'elle n'a pas d'angle (donc qu'au moins 2 points de la courbe sont au même endroit à $t=a$ et $t=b$), alors c'est une géodésique fermée stable. En fait, ça veut dire que la courbe ne bouge plus, et donc est à un minimum local.

### Efficiently Computing Exact Geodesic Loops within Finite Steps[^2]

- Les géodésiques fermées sont cruciales dans l'étude de la **topologie différentielle** et dans la **géométrie différencielle**.

- Contrairement aux autres algorithmes, celui-ci est exact et n'a pas besoin de seuil et est plus performant (de l'ordre des millisecondes pour un maillage d'environ 50 000 sommets.

### The lenght of the shortest geodesic loop at a point (Regina Rotman)[^3]

- Le fait qu'il existe une géodésique fermée sur chaque variété Riemannienne a été montré par L. Lusternik et A. Fet.

### Choking Loops on Surfaces (Xi Feng and Yiying Tong, iEEE)[^4]

#### Détection de topologies non-triviales
**Préprocess:** Tétrahédralisation du maillage, détermination de la carte des distances (trois façons: *Closest point transform*, l'*algorithme de Dijkstra* ou le *fast matching*)
**Entrée:** Un maillage d'objet
**Résumé:** On filtre le maillage, une fois que tout est passé dans la filtration on obtient des homologies. On ajoute les sommets de l'homologie à la filtration, le sommet avec le plus petit $d$ (voir ci-dessous) et ses faces connectées, les simplices du maillage. On répète tout ça jusqu'à ce qu'on obtienne un $d_{max}$. Toutes les faces qui on une surface supérieure à $\delta_{max}$ et leur paire négative seront des *choke face*.
**Sortie:** Une membrane

**Valeurs notables & définitions:**
* $d$: distance de chaque sommet à la surface (enregistré à chaque sommet intérieur)
* $\delta$: persistance de l'objet/face/... qui ignore le bruit (car les maillages sont imparfaits)
* $p-simplices$: regroupement d'objet géométrique p+1, 0 pour les sommets, 1 pour les arrêtes, 2 pour les faces et 3 pour les tétrahèdres

#### Calcul des loops associées
**Entrée:** La membrane obtenue précédemment
**Résumé:** On transforme la membrane jusqu'à avoir une membrane minimale. On ajoute une *choke face* avec son groupe à la membrane.
* On prend le sommet avec le plus grand $d$ de la $boundary$
* On trouve une membrane locale optimale
* On fusionne les membranes.
On répète ces étapes jusqu'à ce que les faces de la $boundary$ soient à la surface.
**Sortie:** Les géodésiques associées à la membrane

**Valeurs notables & définitions:**
* $d$: distance de chaque sommet à la surface (enregistré à chaque sommet intérieur)
* $\delta$: persistance de l'objet/face/... qui ignore le bruit (car les maillages sont imparfaits)
* $boundary$: en topologie c'est genre les points "au bord" de l'objet, qu'on peut "approcher" à la fois via l'intérieur et via l'extérieur)

### Rapport irl de Blumentals[^5]


### From a Closed Piecewise Geodesic to a Constriction on a Closed Triangulated Surface[^6]

Une courbe fermé c dont pour tout les point s, sommets de tetrahèdre de l'objet traversé par c, est une constriction si tous les angles en des points s sont supérieurs ou égaux à $\pi$. Entre les sommets, la courbe reste droite. Ainsi une cette définition est utilisé lors du système dit de "glissement" d'une courbe après avoir trouvé une courbe par simplification/désimplification. On teste si tous les angles sont valides. Si c'est lecas alors on à trouvé une géodésic, sinon on modifie un petit peux notre courbe en recommancant le test jusqu'a trouver la geodesic. On modifie alors la courbe sur le point non convexe afin de trouver une geodesic. On enregistre toutes les positions déjà testées dans un tableau affin de ne pas retomber sur la même lors du petit déplacement de la courbe. //à compléter.

Il existe 2 types de géodesic remarquables. Les géodésics qui coupent l'objet en 2 parties sont du premier type. Les geodesics qui coupent l'espace privé de l'objet sont le second type. Une constriction peux être d'aucun de ces 2 types. Lors de la découverte d'une géodésic par simplification, la géodesic est un triangle de 3 point. Avec cet alogorythme on ne peux trouver des géodesics que des 2 types particuliers. Pour trouver le type on trace une demi-droite aléatoire de base le centre du triangle. On calcule le nombre d'intersection entre cette droite et la surface de notre objet. Si ce nombre est impair alors la géodesic est de premier type, si ce nombre est pair alors elle est du second type. //à completer aussi

### Detection of Constrictions on Closed Polyhedral[^7]

L'algorithme consiste à simplifier la surface jusqu'à obtenir un **triangle qui ne fait pas partie de la surface** (équivalent à une arrête qui, si elle était supprimée, changerait la **topologie de la surface** (en fait, ça couperait la surface en deux). Cette arrête permet de trouver la courbe de départ (**seed curve**), à partir de laquelle nous allons trouver la **constriction, ou géodésique fermée**.

On retourne ensuite progressivement à la forme d'origine à l'aide d'un autre algorithme, tout en mettant à jour la constriction (localement), jusqu'à retrouve la surface de départ.

La constriction passe par un nombre limité mais non nul de sommets, appelés **sommets pivots**. La constriction est une ligne droite entre les sommet, elle effectue des angles seulement sur les sommets qu'elle traverse.

- L'algorithme utilisé pour simplifer la surface est l'algorithme de **contraction des sommets** qui fusionne simplement 2 sommets, jusqu'à obtenir la surface simplifée.

- Les géodésiques fermées sont utilisées de manière pratique pour : robot motion planning, shape analysis, or terrain navigation.

Pour calculer (et metttre à jour) la constriction, on utilise l'algorithme de Pham-Trong, qui donne une approche progressive (itérative). C'est la méthode qui consiste à faire évoluer la courbe sur la surface.

- Moi je dis, quand on présente le sujet, on peut utiliser less notations du
papier

**Trucs pointus sur l'algo**

On note $T^0$ la surface d'origine, on la simplifie, à chaque étape obtient $T^k, T^{k+1}$ etc... Jusqu'à $T^n$ qui est la surface au moment ou la simplification par le triangle qui n'appartient pas à la surface changera la topologie de celle-ci

À partir de ce triangle, on a la **seed curve** $\beta^n$.

On dé-simplifie la surface, donc on retourne à $T^{n-1}, T^{n-2}$ etc... En calculant $\beta_{n-1}, \beta_{n-2}$ etc...

On suppose que pour obtenir $T^{k+1}$, on a fusionné les sommets $v_1$ et $v_2$ de $T^k$ en un sommet $\overline{v}$ de $T^{k}$.

Alors on note $d_{k+1}$ l'ensemble des lignes polygonales qui relient deux **sommets pivots** de $\beta_{k+1}$ et qui n'ont pas d'intersection avec l'ensemble des faces incidentes à $\overline{v}$. Ces lignes n'ont donc **pas besoin d'être misent à jour** car elles ne seront pas affectées par la séparation de $\overline{v}$ en $v_1$ et $v_2$.

On note la courbe $c_{k+1}$ comme $\beta_{k+1} \backslash$ (privé de) $d_{k+1}$.
Pour cette courbe, l'intersection de $c_{k+1}$ avec l'ensemble des faces incidentes au sommet $\overline{v}$ n'est pas vide, donc la courbe sera affectée par la séparation du sommet et devra donc être **mise à jour**.

Au final, on décrit $\beta_k$ comme $d_{k+1} \cup c_k$.

$\rightarrow$ Ce que j'ai compris c'est que la courbe peut être (ou est) recalculée partiellement à chaque étape. Par exemple, si $c_{k+1} = \emptyset$ alors $\beta_{k+1} = \beta_k = d_{k+1}$

**La suite explique comment la courbe $c_k$ est calculée.**


### Constriction Computation using Surface Curvature[^8]

L'ancien algorithme ne permettait pas de détecter certains types de godésique, comme les géodésiques "orthogonales", à cause de la simplification de la forme, qui omet certains géodésiques voisines. Cet algorithme est capable de détecter ces géodésiques.

Avec cet algorithme, pour trouver des sommets initiaux (pour ensuite vraiment calculer les géodésiques), on se sert d'un seuil défini par l'utilisateur. L'algorithme sélectionne tous les sommets dont la somme des angles des surfaces adjacentes est inférieure ou égales au seuil.

À partir de cette seléction de sommets, on créé les courbes initiales en faisant passer un plan à travers la surface qui passe par le sommet sélectionné. (On choisi les directions du plan selon certains critères (3.2).

Le nombre de constrictions finales est souvent différent du nombre de courbes initiales car les courbes initiales peuvent glisser jusqu'au même endroit.

L'article parle d'utiliser l'alrogrithme pour l'appliquer à l'animation de personnages (pour détecter les points d'articulation).

### Morse theory, closed geodesics, and the homology of free loop spaces [^9]

Les **free loops** sont des boucles qui n'ont pas de point de départ.

[Simply-connected manifolds](https://en.wikipedia.org/wiki/Simply_connected_space)

### Efficiently Computing Exact Geodesic Loops within Finite Steps

Des chercheurs ont trouvés que beaucoup de problèmes d'imagerie en informatique (computer graphics) dépendent du calcul de géodsiques fermées.

Premier algo qui permet de calculer des géodésiques exactes en un nombre d'étapes fini, les autres sont plus coûteux, et ne calculent que des géodésiques approximatives.

Ou alors les autres algos auraient besoin d'un temps de calcul infini pour trouver des géodésiques exactes.

Sinon c'est le principe du glissement mais poussé, et rendu plus rapide et efficace. Mais les courbes initiales ne sont pas trouvées par l'algorithme, elles sont spécifiées par l'utilisateur.

On défini un plus court chemin local comme une suite de sommets et de séquences d'arrêtes. (chacunes des séquences d'arrêtes et séparée par un sommet).

(J'en suis au lemme 4)







-----

Liens du seafile : <https://seafile.unistra.fr/d/e83210b1a33a43478935/>

----

[^1]:<https://www.encyclopediaofmath.org/index.php/Closed_geodesic>

[^2]:<https://ieeexplore.ieee.org/abstract/document/5928345)>

[^3]:<http://www.math.toronto.edu/rina/loopsjdg.pdf>

[^4]:<https://seafile.unistra.fr/d/e83210b1a33a43478935/files/?p=/feng_tong-tvcg2013.pdf>

[^5]:<https://seafile.unistra.fr/d/e83210b1a33a43478935/files/?p=/Rapport_irl_blumentals.pdf>

[^6]:<https://seafile.unistra.fr/d/e83210b1a33a43478935/files/?p=/hetroy_attali-pg2003.pdf>

[^7]:<https://seafile.unistra.fr/d/e83210b1a33a43478935/files/?p=/hetroy_attali-vissym2003.pdf>

[^8]:<https://seafile.unistra.fr/d/e83210b1a33a43478935/files/?p=/hetroy-eg_short2005.pdf>

[^9]:<https://seafile.unistra.fr/d/e83210b1a33a43478935/files/?p=/oancea-arxiv2014.pdf>
