- 10 min par groupe

- Présenter le sujet : bien faire comprendre quel est le sujet, les enjeux, le contexte (assez long, 3-5 min).

- Les sources (3 sources minimum, 4-6 min).
	- origine
	- type
	- pertinence par rapport au sujet
	- ce qu'elles apportent par rapport au sujet
	- éventuelles difficultés rencontrées
	- ne pas raconter les résultat des recherches
	- comment on a mené la recherche biblio ?

- Vision critique (1-2 min):
	- étendu de la recherche (est-ce qu'on a tout fait ?)
	- profondeur de l'étude
	- qu'est-ce qu'on va faire pour finaliser l'étude (l'étude bibliographique)
