SRC=$(wildcard *.md)
PDF=$(SRC:.md=.pdf)

.PHONY: all clean

all: $(PDF)

%.pdf : %.md
	pandoc --pdf-engine=xelatex $< -o $@

Presentation/Presentation.pdf: Presentation/Presentation.md
	pandoc $< -t beamer -o $@

Presentation/Discours.pdf: Presentation/Discours.md
	pandoc $< -t beamer -o $@

clean:
	rm *.pdf
