---
title : "Rapport de Projet de Recherche et de Documentation Scientifique"
author: "Audric Besnault--Clérice, Raphaël Colin, Morgane Schieber"
geometry : margin=3cm
fontsize: 12pt

header-includes:
  \usepackage{graphicx}
---

\title{
  \begin{center}
      \includegraphics[height=7cm]{figures/etrange_objet.png} \\~\\
  \end{center}
    \begin{center}
    \Huge{Propriétés et Calcul des Géodésiques Fermées}
    \end{center}
  \begin{figure}[b]
    \begin{center}
      \includegraphics[height=4cm]{figures/logo.jpeg}
    \end{center}
  \end{figure}
}

\newpage

\renewcommand{\contentsname}{Table des matières}
\tableofcontents

\newpage

# Introduction

Le terme "géodésique" est utilisé pour décrire le plus court chemin entre deux
points d'une surface courbe. L'étude de ces chemins joue un rôle fondamental
dans de divers domaines, notamment dans l'étude des distances sur la Terre
(géodésie), d'où ils tirent leur nom.

Par la suite, les géodésiques furent étudiées d'un point de vue mathématique, ce
qui donna naissance à un type particulier de géodésiques : les **géodésiques
fermées**.

Une géodésique fermée est simplement le chemin le plus court d'un point vers
lui-même (en excluant le chemin vide). En pratique, ce sont des courbes qui
entourent les objets.

Les plus intéressantes à calculer sont les géodésiques fermées qui représentent
des minima locaux, c'est-à-dire celles qui sont plus courtes que toutes les autres
autour, car elles permettent de séparer les objets en trois dimensions en des
parties plus simples de manière naturelle. En effet, lorsque l'on observe un
objet, il est facile d'imaginer des séparations au niveau des points les plus
étroits.

Un exemple de ce type de géodésique est proposé par la figure 1.

![Des géodésiques fermées les plus courtes localement dessinées sur un pichet](figures/pichetb2.png)

Tout au long de ce rapport, nous allons tenter de fournir une vision des
géodésiques fermées à un niveau mathématique, et nous parlerons des méthodes
de calcul qui s'y rapportent.

Nous commencerons par exposer les différents enjeux liés à ce sujet, tant au
niveau des domaines d'application possibles que des problèmes de calcul
inhérents au sujet. Nous dresserons ensuite un aperçu des géodésiques fermées
d'un point de vue mathématique pour enfin conclure sur les problèmes
informatiques posés par ces courbes, et les manières de calculer ces
dernières.


\newpage

# Enjeux

## Domaines d'application

Le sujet des géodésiques fermées peut sembler être un sujet de niche, réservé
à quelques chercheurs, et qui n'a aucune incidence sur le reste de la recherche,
ou sur d'autres domaines qui s'éloignent des mathématiques ou de l'informatique.

Nous allons voir que, au contraire, les enjeux des travaux sur le sujet
dépassent largement le cadre limité de la recherche en informatique, ou même
le cadre de la recherche tout court.

Parmi les domaines d'application possibles, on compte notamment la médecine,
où les géodésiques permettent la détection d'arythmies cardiaques. Pour ce
faire, une modélisation du cœur sera réalisée, afin de provoquer des stimulations
à des endroits non-stimulés en temps normal. Cela va provoquer des flux d'ions
dont la diffusion sera calculée, ce qui va mettre en lumière les zones supposément
sujettes à être la cause de l'arythmie.
De nombreuses courbes seront tracées sur ces zones, et seules celles qui
correspondent à des géodésiques fermées seront retenues.
Parmi ces courbes candidates, la plus courte correspondra à la région
responsable du problème.

![Une géodésique fermée calculée sur un cœur](figures/coeur2.jpg)

Il existe également des applications plutôt orientées vers l'industrie. Les
géodésiques peuvent servir à repérer les parties plus faibles que les autres
dans un système mécanique, les parties où se trouvent les géodésiques
étant moins résistantes, donc plus sujettes à la casse que les autres.

Le domaine de l'exploitation pétrolière peut aussi grandement bénéficier de
l'étude de ces courbes, où elles permettent de trouver l'endroit le plus optimal
pour forer. Il n'est pas possible de pouvoir récupérer absolument tout le pétrole
d'une poche, c'est pour cela que l'on cherche le point optimal où forer, les
géodésiques permettent sa localisation.

Enfin, nous pouvons noter leur possible utilisation dans un domaine plus
artistique, celui de l'animation 3D, où leur présence permet de faciliter
l'animation des articulations.
Elles permettent d'assister les infographistes et animateurs dans le placement
de points d'articulation sur des modèles 3D pour l'animation de personnages.

## Représentation Informatique

Un autre enjeu lié aux géodésiques fermées est la difficulté de leur calcul
dans un cadre informatique. En effet, les surfaces lisses et "simples" à utiliser
que l'on peut représenter en mathématiques sont beaucoup plus difficiles à
manipuler dans un contexte informatique, où ces mêmes surfaces sont modélisées
par des maillages, un ensemble de tétraèdres qui permettent la représentation
d'objets 3D en machine.

Lorsque le travail doit être effectué sur des maillages, de nombreux problèmes
se posent :

Premièrement, certaines propriétés qui sont vraies dans un contexte continu
en mathématiques ne peuvent plus l'être dans un contexte discret comme celui des
maillages, auxquels nous sommes limités.

Cette représentation des objets en 3 dimensions pose également un problème de
"bruit".
Le mot "**bruit**" est utilisé pour décrire des données non significatives qui
peuvent fausser des résultats. Dans notre cas, le **bruit** fait référence à
toutes les imperfections de la surface maillée qui peuvent fausser le calcul des
géodésiques. Il est donc naturel que l'on souhaite ignorer ce bruit lors des
calculs des géodésiques. La véritable question devient alors : comment
distinguer ce qui n'est pas du bruit de ce qui est effectivement du bruit ?

Il est possible de le définir arbitrairement, de demander une valeur de seuil à
l'utilisateur, ou encore tout simplement de ne pas chercher à l'éviter.
Néanmoins, ces solutions restent insatisfaisantes dans la plupart des cas, mais
nous aborderons ce sujet plus en détails dans la suite du rapport.

Enfin, les maillages occupent une grande place en mémoire, surtout pour les
objets les plus détaillés, et encore plus pour certains algorithmes qui gardent
plusieurs maillages en mémoire.

\newpage

# Mathématiques

Afin de créer des algorithmes complexes, nous avons besoin de propriétés
mathématiques sur lesquelles baser nos calculs. L'étude des géodésiques fermées sur un
plan algorithmique se doit donc d'être lié à leurs études mathématiques. Nous
allons définir les notions importantes pour l'informatique ainsi que les
propriétés utilisées dans les algorithmes.

## Définir mathématiquement une géodésique fermée

Les géodésiques sont définies dans des espaces en trois dimensions. Nous voulons
les étudier sur des surfaces imparfaites et imprévisibles. Ces objets sont
appelés surface courbe, plus précisément ce sont des espaces Riemanniens sur
lesquels s'appliquent la géométrie riemannienne. Il nous faut donc commencer
par définir ces espaces.

**Géométrie Riemannienne :** La géométrie des espaces courbes de dimension $n$ sur
lesquels les calculs d'angles et de longueurs sont possibles.

Pour la suite, on se place dans un espace Riemannien noté $E$ de dimension 2,
représentant la surface d'un objet de dimension 3. Cet espace représente le
point de vue qu'un observateur se trouvant sur la surface de l'objet pourrait
avoir.


Pour tout $C$, courbe de l'espace $E$, notons
$L(C)$ la longueur de la courbe $C$.

Comme les longueurs sont calculables, il est possible de calculer les chemins les plus
courts. Les géodésiques sont les chemins les plus courts entre 2 points
dans un espace Riemannien, elles peuvent être vues comme la
généralisation des droites sur des espaces courbes.

Soit $C_A^B$ l'ensemble de toutes les courbes $c$ qui relient $A$ et $B$ et où
$L(c) \neq 0$. On a alors les définitions suivantes :


Une géodésique de $A$ vers $B$ est une courbe $\gamma \in C_A^B$ telle que
$\forall c \in C_A^B, L(\gamma) \leq L(c))$.


Dans l'espace $E$, la géodésique fermée en un point $A$ est la courbe de distance
minimale qui entoure l'objet en suivant sa surface. Ainsi une géodésique fermée
est plus courte que toutes les autres boucles passant par $A$.

Une géodésique fermée $\gamma \in C_A^A$ est telle que $\forall c \in C_A^A,  L(\gamma) \leq L(c)$.

Soit $A$ un point et $\gamma$ une courbe fermée : G($\gamma$,A) signifie que $\gamma$
est une géodésique fermée de $A$.
On appelle géodésique périodique une boucle qui est une géodésique fermée
pour tous les points lui appartenant.

Une géodésique périodique $\gamma \in E$ est telle que $\forall A \in \gamma, G(\gamma,A)$

Les constrictions sont un cas particulier de géodésique périodiques. Elle
représentent la courbe la plus courte localement.

Soit $\alpha$ un point de la surface, $\epsilon \in \mathbb{R}$  et $\gamma$ une courbe telle que
$G(\gamma, \alpha)$. Définissons l'ensemble  $d(\gamma, \epsilon)$ comme ceci :

$d(\gamma,\epsilon) =  \{X \in E | min(\bigcup\limits_{\alpha \in \gamma}(\bigcup\limits_{c \in
C_{\alpha}^X}(L(c)))) \leq \epsilon\}$.

Une constriction est une courbe $\gamma \in C_A^A$ telle que $\exists \epsilon \in \mathbb{R}, \epsilon
> 0$ tel que $\forall X \in d(\gamma,\epsilon), \forall l \in C_X^X ,L(l) > L(\gamma)$

Dans le cadre de ce rapport nous utilisons les termes "géodésiques
fermées" et "constrictions" comme des synonymes afin de parler de constrictions.
Cette assimilation se constate déjà dans de très nombreux articles de recherche.

Nous allons également utiliser le mot "géodésique" pour désigner les "géodésiques
fermées" dans le reste du rapport.

## Propriétés mathématiques sur les maillages

Dans le cadre de l'informatique nous utilisons un maillage tétraédrique.
L'espace devient alors un espace discret, et les
propriétés ne sont donc pas identiques. Dans ce cadre, nos courbes sont décrites
par un enchaînement de faces et de sommets traversés. Un sommet $A$ traversé par
une courbe $\gamma$ est nommé un *sommet pivot de $\gamma$*.

Une constriction, pour répondre à sa définition, doit être moins longue que toutes
les autres courbes proches. Ainsi, elle est obligée de passer dans une partie
concave de l'espace, sinon une autre courbe proche serait plus courte.
Étant dans un espace courbe, la
somme des angles en un point peux différer de $2\pi$. Ainsi, on peut définir si un
point est dans un creux en calculant les 2 angles créés par la courbe. Si le
minimum de ces 2 angles, noté $An(a)$, est inférieur à $\pi$, notre sommet est un
sommet placé dans une région concave.

$\gamma$ est une constriction $\iff \forall a \in \gamma, An(a) \geq \pi$

Un algorithme se base sur une propriété non démontrée actuellement :l'idée serait
qu'une constriction est planaire. Cette idée est intuitive mais aucune
démonstration n'a été trouvée, que ce soit dans un espace discret ou continu.

Nous tenons à mentionner les articles suivants qui nous ont permis de comprendre
l'aspect mathématique de notre sujet : [^1] [^2] [^5].

\newpage

# Informatique

## Principe des algorithmes

Il existe de nombreux algorithmes permettant de calculer les courbes
géodésiques d'un objet en 3 dimensions. Comme nous l'avons évoqué précédemment,
les objets en 3 dimensions sont représentés par des maillages en machine, ces
algorithmes prennent donc comme entrée un maillage, et fournissent en sortie
l'ensemble des courbes géodésiques qui ont été calculées.

La plupart de ces algorithmes reposent sur le même principe que nous allons
détailler dans la partie suivante : Ils cherchent une courbe initiale, puis
effectuent une opération de glissement qui permet de déplacer cette courbe à la
position exacte d'une géodésique. Plusieurs courbes initiales sont calculées,
elles se rejoignent parfois en la même géodésique, ou peuvent donner plusieurs
géodésiques distinctes.

Ce qui détermine un bon algorithme pour ce problème est particulier :
en plus d'avoir l'algorithme le plus rapide et le plus économe en mémoire, il
faut que celui-ci trouve toutes les géodésiques pertinentes.

Il est difficile de définir sur un maillage quelles géodésiques sont
pertinentes, et lesquelles sont dues au problème du bruit que nous avons
évoqué plus haut. En fait, cette difficulté vient directement du fait qu'il est
impossible de définir mathématiquement quelles géodésiques sont "pertinentes"
et lesquelles ne le sont pas, notamment car la notion de pertinence d'une
géodésique est tout à fait humaine.

La figure 3 illustre une instance où le bruit du maillage provoque la
détéction d'une géodésique en trop (la géodésique bleue). Là où seule la géodésique
rouge devrait être détectée.

![Détection d'une géodésique provoquée par le bruit](figures/vase6b2.png)

Nous allons voir quelles ont été les solutions de divers algorithmes pour
déterminer quelles courbes initiales sont pertinentes et lesquelles ne le sont
pas. Il faudra également comparer ces solutions entre elles et tenter
d'expliquer pourquoi chaque algorithme utilise sa méthode plutôt qu'une
autre.


## Trouver une courbe initiale

La première étape pour la plupart des algorithmes de calcul des constrictions
est de trouver un bon candidat pour une courbe initiale. En fait, plusieurs
courbes initiales sont sélectionnées, mais nous allons décrire les algorithmes
pour une seule courbe ici, le procédé étant le même à chaque fois.

### Simplification de la surface

Une première méthode que l'on peut imaginer afin de détecter une courbe initiale
est celle de la simplification du maillage.

Cette méthode est celle décrite dans l'article *"From a closed peacewise
geodisic to a constriction on a closed triangulated surface*"[^1].

Cette méthode consiste à simplifier l'objet en utilisant l'algorithme de *edge
collapse* qui consiste à fusionner deux sommets voisins à chaque étape de
simplification.

La condition d'arrêt pour la simplification est tout simplement la détection
d'un triangle qui ne fait pas partie d'un des tétraèdres du maillage (en fait,
si la surface était simplifiée encore une fois, cela modifierait
la topologie de l'objet, c'est-à-dire le scinderait en deux dans la
plupart des cas).

Nous pouvons observer la détection d'un tel triangle sur la figure 4.

![Détection d'une courbe initiale sur une surface simplifiée](
figures/maillage_simplifie2.png)

Une fois cette courbe initiale détectée, il nous faut restaurer l'objet à son
état de départ sans perdre cette même courbe. Pour ce faire, il suffit
de séparer les sommets qui ont été fusionnés lors de l'étape de simplification,
tout en réajustant les sommets pivots par lesquels passe la courbe initiale à
chaque étape selon certaines règles.

Une fois l'objet dans son état initial, il suffit d'effectuer l'algorithme
de glissement expliqué plus tard afin de trouver une constriction.

Cet algorithme est l'un des premiers à être paru pour le calcul de géodésiques
fermées, mais possède beaucoup de défauts, notamment le fait que certaines
courbes initiales ne peuvent pas être détectées à cause du procédé de
simplification, ou encore le coût en mémoire qui est conséquent. En effet, il
est nécessaire de garder en mémoire les séparations de sommets pour pouvoir
raffiner l'objet par la suite, sans parler de la complexité en temps de
l'algorithme.

### Sélection à l'aide de la courbure de la surface

Une autre méthode qui est apparue légèrement plus tard consiste à étudier la
courbure (les zones concaves ou convexes) de la surface afin de déterminer
une courbe initiale.

Cet algorithme est décrit dans le papier "*Constriction Computation using
Surface Curvature*"[^2], qui est plus court que l'article précédent, et
dont l'algorithme peut détecter d'autres géodésiques (figure 5).

![Un type de constriction détectable avec ce nouvel algorithme : les
constrictions orthogonales](figures/new_geodesics2.png)

La détection de courbes initiales ici va se dérouler en deux parties :

Premièrement, il s'agit de sélectionner des sommets candidats. Il faut ensuite
créer un plan qui intersecte la surface, ce qui va nous permettre d'obtenir une
courbe.

Les sommets candidats sont choisis en fonction de la somme des angles qu'ils
font avec les faces adjacentes. En fait, cela revient à dire que ces angles
doivent se trouver dans des zones **concaves** : plus la somme des angles est
élevée, plus la zone est concave, et donc plus cette zone a des chances de
comporter une constriction.

Cependant, il existe beaucoup de sommets potentiellement candidats sur un
maillage, surtout s'il est complexe, ce qui pourrait donc mener à beaucoup de
géodésiques non-pertinentes, et donc un algorithme très mauvais.
Pour résoudre ce problème, la solution qui a été choisie est de ne sélectionner
que les points ayant une somme des angles supérieure à un certain seuil
spécifié par l'utilisateur. Cette solution reste limitée, mais permet
manuellement de filtrer les géodésiques calculées.

Une fois qu'un sommet candidat a été détecté, il reste à trouver une courbe
initiale en partant de ce sommet : ici, un plan est tracé depuis ce dernier.
Ce plan coupe la surface, ce qui permet tout naturellement de tracer une
courbe sur le maillage, avant d'appliquer l'algorithme de glissement.
Cette méthode se base en fait sur l'assomption qu'une géodésique fermée est une
courbe planaire, c'est à dire qu'elle suit un plan. Nous avons vu que cette
propriété n'était pas démontrée dans la partie sur les propriétés mathématiques
des géodésiques, mais elle reste relativement intuitive.

Cet algorithme ne permet toujours pas de calculer toutes les constrictions d'un
objet et reste assez coûteux en temps. On peut également questionner le choix
d'utiliser une propriété non-démontrée sur les géodésiques. En revanche, il est
capable de détecter plus de géodésiques que le premier et utilise beaucoup
moins de mémoire. Enfin, nous pouvons nous questionner sur le choix d'utiliser
un seuil défini par l'utilisateur, mais nous aborderons ce type de questions
plus loin...

### Calcul par tétraédrisation

Parmi les algorithmes que nous avons pu étudier, il a celui du papier
"*Choking Loop on Surfaces*"[^3]. Les bases
mathématiques de cet algorithme sont assez complexes et touchent beaucoup à la
topologie (comme la plupart des algorithmes de ce genre). Il est constitué de
deux
grandes parties : une qui consiste à transformer le maillage en une **membrane**, et
l'autre qui consiste à transformer cette membrane afin d'y localiser les géodésiques.

**Transformation en membrane**

Pour commencer, le maillage de l'objet est tétraédrisé. La tétraédrisation
consiste à remplir l'objet de tétraèdres à partir de la surface, jusqu'à ce que
le tout se rejoigne à l'intérieur. L'objet est ensuite simplifié de différentes
façons (qui dépendront du type d'objet). Une carte de distances est alors obtenue,
et c'est cette carte qui sera filtrée jusqu'à l'obtention d'un objet transformé
qu'on appelle une **membrane**. La figure 6 illustre cette
tétraédrisation.

**Recherche de géodésiques**

Sur la membrane obtenue précédemment il y a des endroits plus susceptibles que
d'autres de présenter des géodésiques. On les appelle les **choke faces**. Divers
calculs et transformations sont effectuées sur cette membrane et ses choke
faces jusqu'à ce que l'on arrive à un "étouffement" (un endroit où les bords de
la membrane se touchent). C'est à ces endroits que se trouvent les
géodesiques.

![Résultat de l'algorithme de Feng & Tong. En jaune: la géodésique
trouvée.](figures/sortie_algo_fengtong.png)

Chaque étape de l'algorithme dure environ une seconde, ce qui, dans son ensemble
prend un certain temps. Les étapes les plus coûteuses sont celles qui utilisent
la persistance des objets obtenus par homologie (ces étapes servent à pallier au
problème de "bruit" dont nous avons parlé précédemment). À l'issue, seules les
géodésiques utiles sont obtenues, celles qui nous
intéressent le plus de manière générale. Mais un problème se pose : si
plusieurs géodésiques fermées sont trop proches l'une de l'autre, l'algorithme
ne nous en donnera qu'une seule. Et il n'y a pas moyen de savoir laquelle.


## L'algorithme de glissement

Nous avons vu comment ces différents algorithmes parviennent à détecter les
courbes initiales qui vont servir à calculer les véritables géodésiques de l'objet.

Là où l'étape précédente était riche en diversité de solutions et de méthodes,
celle-ci a tendance à mettre tout le monde d'accord.

Même si les algorithmes utilisés en pratique peuvent varier, et que certaines
versions fonctionnent mieux que d'autres, le principe du **glissement**
demeure le même au travers de tous les algorithmes vus précédemment.

Imaginons que la courbe initiale que nous avons calculée est un élastique qui ne
peut que se resserrer. Il est forcé de se déplacer sur l'objet, mais ne peut
naturellement pas se déplacer d'un côté qui serait plus large que son diamètre,
car cela nécessiterait qu'il puisse s'élargir. Au final, l’élastique se
déplacera du côté le plus étroit jusqu'à ne plus pouvoir bouger. C'est à ce
moment là que l'on a détecté une constriction. Pour ce faire, la propriété des
angles est utilisée. Lorsqu'une courbe est obtenue, on regarde si tous ses sommets pivots
valident la propriété. Si un sommet ne la valide pas, on modifie la courbe pour
qu'elle passe par un autre sommet proche dans la direction où l'angle était inférieur à $\pi$.
Cette étape est appliquée jusqu'à ce que tous les sommets pivots soient corrects.
Une constriction est alors trouvée.

![Géodésiques fermées calculées sur une statue (algorithme de Feng & Tong)](
figures/bruit2.png)

Notons que la notion de bruit intervient ici également. En effet, comme le
montre la figure 7, le moindre creux causé par le maillage sur la
surface immobiliserait la courbe, et donnerait comme résultat une géodésique
jugée "non pertinente". Nous pouvons voir ici que certaines géodésiques très
proches sont calculées (sur les bras ou le tronc), qui devraient se résumer à
une seule géodésique chacune.

\newpage

## Réflexions sur la pertinence du calcul de courbes initiales

Nous avons pu voir que certains algorithmes se reposaient sur une entrée de
l'utilisateur afin de calculer des courbes initiales.

Certains chercheurs ont simplement trouvé que le calcul d'une courbe initiale
n'était pas nécessaire. C'est le cas de Shi-Qing Xin, Ying He et Chi-Wing Fu
auteurs du papier "*Efficiently Computing Exact Geodesic Loops within Finite
Steps*"[^4].

L'algorithme décrit dans ce papier consiste en un glissement
similaire à celui expliqué dans la prochaine partie, qui s'inspire beaucoup
de celui décrit dans le premier article que nous avons présenté. Il n'y a aucune
étape de calcul de courbe initiale, celle-ci étant spécifiée par l'utilisateur.

Cela nous amène à nous questionner sur la pertinence du calcul de cette courbe
de départ.

Le concept d'une constriction est très humain, on peut en général voir au
premier coup d'œil si une courbe en est une ou non. Il est donc plutôt pertinent
pour certaines applications des géodésiques de laisser le soin à l'utilisateur
de choisir une courbe initiale, qui sera ensuite placée à l'endroit exacte de la
géodésique par l'ordinateur. En effet, dans le domaine de l'animation par
exemple, l'utilisateur pourrait indiquer les endroits approximatifs des
articulations de son modèle, où les géodésiques seraient ensuite précisément
placées par l'algorithme.

Cette solution a des limites cependant, par exemple pour calculer un grand
nombre de géodésiques fermées sur un objet très complexe.

Nous avons dressé un panorama assez divers des algorithmes permettant de
calculer une courbe initiale. Nous allons désormais nous intéresser à la
transformation de cette courbe en une véritable constriction grâce à
l'algorithme de glissement.

\newpage

# Conclusion

Nous avons pu voir en quoi les géodésiques fermées constituaient un sujet
important de la recherche, ainsi que de nombreux domaines d'application dans
lesquels le calcul de ces courbes a un véritable enjeu.

Nous avons également remarqué que les géodésiques fermées sont des objets finalement
assez simples à se représenter mathématiquement, et assez simple à manipuler
dans ce contexte, mais qui deviennent très difficiles à utiliser dans un
contexte discret tel que les maillages.

Après avoir fait un panorama des manières de calculer une courbe initiale pour
différents algorithmes, nous avons décrit l'algorithme de glissement que partagent
toutes ces méthodes. Enfin, nous avons discuté d'un angle de prise de recul
sur le sujet : la pertinence du calcul d'une courbe initiale


Au final, les géodésiques n'ont pas qu'un impact sur les domaines où elles sont
utiles, mais nous font réfléchir sur la transition entre mathématiques et
informatique à travers l'exemple de la représentation des surfaces de Riemann
par des maillages. Grâce aux géodésiques, nous pouvons nous rendre compte de la
difficulté d'une telle traduction, et des différences fondamentales entre
mathématique et informatique, deux domaines qui semble pourtant si proches l'un
de l'autre.




[^1]:(Franck Hétroy, Dominique Attali 2003) <https://hal.inria.fr/inria-00001145/file/hetroyf_constriction.pdf>
[^2]:(Franck Hétroy 2005) <https://hal.inria.fr/inria-00001135/file/Het05.pdf>
[^3]:(Xing Feng Yiyin Tong 2013) <https://www.cse.msu.edu/~ytong/papers/chokingLoops.pdf>
[^4]:(Xin, He, Fu 2011) <https://ieeexplore.ieee.org/abstract/document/5928345>
[^5]:(Encyclopedia of math, Closed Geodesic) <https://www.encyclopediaofmath.org/index.php/Closed_geodesic>
