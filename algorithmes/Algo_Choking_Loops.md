---
title: "Choking Loops on Surfacese"
---

## Input

L'algo prend en entrée un maillage d'un objet (surface triangulée).

## Output

Certaines géodésiques fermées sur le maillage.

## Idée principale

Transformer l'objet en une membrane (homologie de l'objet de base) sur laquelle on va chercher des géodésiques fermées.

## Algorithme

### Transformation en membrane

On va passer un maillage en entrée de cet algorithme. Il va être tétrahedralisé. La tétrahedralisation consiste à remplir l'objet de tétrehèdres à partir de la surface, jusqu'à ce que le tout se rejoigne à l'intérieur. Après ça ils vont simplifier l'objet de différentes façon (qui dépendra du type d'objet). Ils auront une carte de distance en sortie. Cette carte sera filtré jusqu'à ce qu'ils obtiennent une **membrane**.

### Recherche de géodésiques

Sur la membrane il y a des endroits plus susceptibles que d'autres d'avoir des géodésiques, ils appellent ça les **choke face**. Ils vont faire des calculs au fur et à mesure sur les choke face jusqu'à trouver les endroits où se trouvent les géodésiques.

### Type de géodésique

Cet algorithme ne trouve que les géodésiques utiles (donc les 1e et 2e types, donc pas les 3e).

&nbsp;

&nbsp;

## Avis/Limitations

- L'algorithme utilise la persistance des objets obtenus par homologie et c'est ce qui prend le plus de temps. Autrement le reste est rapide (environ 1 seconde par étape).
- Un soucis rencontré: si il y a plusieurs géodésiques fermées trop proches les unes des autres, l'algorithme n'en donnera qu'une (on ne choisit d'ailleurs pas laquelle).
