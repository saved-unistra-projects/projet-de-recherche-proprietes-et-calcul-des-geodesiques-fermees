---
title: "Constriction Computation using Surface Curvature"
---

## Input

L'algo prend en entrée un maillage d'un objet (surface triangulée).

## Output

Certaines géodésiques fermées sur le maillage.

## Idée principale

Selections de points sur le maillage, On fait passer un plan à traves la surface en ce point.
On applique ensuite l'algorithme de glissement sur les courbes affin de trouver
les géodésiques fermées stables.

## Pipeline

### Trouver des points

Les sommets initiaux sont trouvées avec la somme des angles autours des points.
Cette somme est calculée pour chaque point et tous les points dont cette 
somme est au dessus d'un seuil choisi à la main sont gardées.

### Choisir un plan par point

Un plan passant par ce point est choisis en fonction de diverts paramètres.
Ce plan dessine alors une courbe sur notre objet qui nous serviras de base pour
le glissement.

### Glissement

Sur chaque courbe l'algorithe de glissement est appliqué.

Même alog de glissement que dans la version de 2003.

Il consiste à regarder tous les sommets par lesquels notre courbe passe, on regarde si ils sont tous concaves (angle supérieur ou égal à $\pi$ ) ou non. Si c’est le cas, c’est une géodésique, sinon, on déplace la courbe. (à préciser) Cette méthode implique de garder en mémoire les sommets que l’on a déjà testépour ne pas tester les même plusieurs fois.

Certaines courbes peuvent se rejoindre et ainsi une géodésiques est trouvé pour
plusieurs calculs.

## Avis/Limitations

-Lourd en terme de temps de calcul

-Trouve des géodésiques non trouvés par le premier Algo

-Ne trouve pas beaucoup de géodésiques

-Se base sur une propriété mathématique non démontrée
