---
title: "From a closed peacewise geodisic to a constriction on a closed triangulated surface"
---

## Input

L'algo prend en entrée un maillage d'un objet (surface triangulée).
 
## Output

Certaines géodésiques fermées sur le maillage.

## Idée principale

Simplifier l'objet pour repérer facilement une courbe de départ, puis retransformer l'objet vers sa forme initiale en ajustant la courbe à chaque fois, puis, lorsque l'on a retrouvé l'objet d'origine, on utilise l'algorithme de glissement pour trouver la géodésique finale.

## Pipeline

### Simplification

On utilise la méthode **edge collapse** (fusion des sommets) pour simplifier le maillage reçu en entrée.

La méthode consiste à fusionner deux sommets adjacents, et donc les surfaces auxquelles les deux sommets apprtiennent pour simplifier le maillage. On applique donc c'est méthode de manière itérative jusqu'à trouver un triangle qui ne fait pas partie du maillage (une sorte de triangle qui entoure la surface), en fait, un triangle qui, si on fusionnait ses points changerait la topologie de l'objet et qui constitue notre **seed curve** (courbe de départ).

### Type de géodésique

À partir de cette **seed curve**, on peut déjà distinguer le type de la géodésique.

Pour celà, on part du **barycentre** des points du triangle, et on trace une demi-droite aléatoire partant de celui-ci.

On compte le nombre d'intersections entre cette demi-droite et le contour de l'objet. Si le nombre d'intersections est pair, c'est une géodésique du second type, sinon c'est une géodésique du premier type.

Le troisième type est indétectable, mais inutile.

Le type des géodésiques se définit en fonction de leur impact sur la **topologie** de l'objet si on les resserait (changement du **genus**).

&nbsp;

&nbsp;

### Raffinement

On retourne ensuite le maillage à sa forme d'origine.

Pour celà, une utilise simplement l'inverse du **edge collapse** :

On sépare les sommets qu'on a fusionné en deux sommets distincts.

Si une partie de notre courbe sera impactée par cette séparation, on recalcule cette partie, sinon, on la laisse telle quelle.

On itère cette opération jusqu'à retrouver notre surface d'origine.

### Glissement

Pour trouver notre géodésique finale, on utilise l'algorithme de glissement.

Il consiste à regarder tous les sommets par lesquels notre courbe passe, on regarde si ils sont tous concaves (angle supérieur ou égal à $\pi$) ou non. Si c'est le cas, c'est une géodésique, sinon, on déplace la courbe. **(à préciser)**

Cette méthode implique de garder en mémoire les sommets que l'on a déjà testé pour ne pas tester les même plusieurs fois.

## Avis/Limitations

- Lourd en termes de mémoire

- Lourd en termes de temps de calculs

- Certains géodésiques de l'objet ne peuvent pas être trouvées à cause du principe de simplification
